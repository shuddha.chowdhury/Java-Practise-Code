public class MarsRobot {
	String status;
	int speed;
	float temparature;
	
	void checkTemparature() {
		if (temparature < -82) {
			status = "returning home";
			speed = 7;
		}
	}
	
	void showAttributes() {
		System.out.println("Status: " + status);
		System.out.println("Speed: " + speed);
		System.out.println("Temparature: " + temparature);
	}

}
