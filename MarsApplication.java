public class MarsApplication {
	public static void main(String[] arguments) {
		MarsRobot spirit = new MarsRobot();
		spirit.status = "exploring the area";
		spirit.speed = 4;
		spirit.temparature = -65;
		
		spirit.showAttributes();
		System.out.println("Inceasing speed to 5.");
		spirit.speed = 3;
		spirit.showAttributes();
		System.out.println("Changing temparature to -95");
		spirit.temparature = -95;
		spirit.showAttributes();
		System.out.println("Checking the temparature");
		spirit.checkTemparature();
		spirit.showAttributes();
	}

}
